// Copyright 2020 YeHaike. All Rights Reserved.

using UnrealBuildTool;
using System.Collections.Generic;

public class BPGScreenshotTarget : TargetRules
{
	public BPGScreenshotTarget(TargetInfo Target) : base(Target)
	{
		Type = TargetType.Game;

		ExtraModuleNames.AddRange( new string[] { "BPGScreenshot" } );
	}
}
