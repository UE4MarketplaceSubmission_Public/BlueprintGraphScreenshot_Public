// Copyright 2020 YeHaike. All Rights Reserved.

using UnrealBuildTool;
using System.Collections.Generic;

public class BPGScreenshotEditorTarget : TargetRules
{
	public BPGScreenshotEditorTarget(TargetInfo Target) : base(Target)
	{
		Type = TargetType.Editor;

		ExtraModuleNames.AddRange( new string[] { "BPGScreenshot" } );
	}
}
